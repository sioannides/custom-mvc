<?php
namespace App;

use Illuminate\Database\Capsule\Manager as Capsule;

class DatabaseConnection
{
    function __invoke(){
        $capsule = new Capsule;
        $capsule->addConnection(
            );
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}

$connection = new DatabaseConnection;
