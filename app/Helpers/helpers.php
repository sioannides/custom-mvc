<?php

    function recursiveLoadFiles($dir){
        $it = new RecursiveDirectoryIterator($dir);
        $it = new RecursiveIteratorIterator($it);
        $it = new RegexIterator($it, '#.php$#');

        return $it;
    }
