<?php

namespace App;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Exception\NoConfigurationException;

class Router
{
    public function __invoke(RouteCollection $routes)
    {
        $context = new RequestContext();
        $request = Request::createFromGlobals();
        $context->fromRequest(Request::createFromGlobals());

        // Routing can match routes with incoming requests
        $matcher = new UrlMatcher($routes, $context);
        try {
            $matcher = $matcher->match($_SERVER['REQUEST_URI']);

            // Cast params to int if numeric
            array_walk($matcher, function(&$param)
            {
                if(is_numeric($param))
                {
                    $param = (int) $param;
                }
            });

            $className = '\\App\\Controllers\\' . $matcher['controller'];
            $classInstance = new $className();

            // Add routes as paramaters to the next class
            $params = array_merge(array_slice($matcher, 2, -1), array('routes' => $routes));

            call_user_func_array(array($classInstance, $matcher['method']), $params);

        } catch (MethodNotAllowedException $e) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 405 Method Not Allowed');
            /*echo 'Route method is not allowed.';*/
        } catch (ResourceNotFoundException $e) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
            /*echo 'Route does not exists.';*/
            require_once APP_ROOT . '/views/errors/404.php';
        } catch (NoConfigurationException $e) {
            echo 'Configuration does not exists.';
            trigger_error("Number cannot be larger than 10");
        }
    }
}

// Invoke
$router = new Router();
$router($routes);
