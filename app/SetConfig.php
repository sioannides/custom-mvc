<?php
namespace App;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;

class SetConfig
{
    public function __construct(){
        // Iterate Config Files
        $it = new RecursiveDirectoryIterator(dirname(__DIR__).'/config/');
        $it = new RecursiveIteratorIterator($it);
        $it = new RegexIterator($it, '#.php$#');

        foreach ($it as $include) {
            if ($include->isReadable()) {
                // require_once Config Files
                $array = require_once($include->getPathname());
                $this->set_constant($array);
            }
        }

    }

    private function set_constant(array $array){
        foreach ($array AS $item => $value){
            //define constants
            define($item, $value);
        }
    }
}

new SetConfig;
