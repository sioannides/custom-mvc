<?php

namespace App\Controllers;

use App\Models\Page;
use Symfony\Component\Routing\RouteCollection;

class PageController
{

    // Show home
    public function indexAction( RouteCollection $routes )
    {
        require_once APP_ROOT . '/views/home.php';
    }


    // Show the page attributes based on the slug.
    public function showAction( string $slug, RouteCollection $routes )
    {
        $page = new Page();
        $page = $page->show($slug);

        require_once $page->getView();
    }
}
