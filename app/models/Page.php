<?php
namespace App\Models;

class Page
{
    protected $id;
    protected $title;
    protected $description;
    protected $image;
    protected $slug;
    protected $view;

    // GET METHODS
    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getView()
    {
        return $this->view;
    }

    // SET METHODS
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }


    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function setImage(string $image)
    {
        $this->image = $image;
    }

    public function setView(string $view)
    {
        $this->view = $view;
    }

    // CRUD OPERATIONS
    public function create(array $data)
    {

    }

    public function show(string $slug)
    {
        if(!empty($page)){
            $this->setView(APP_ROOT . '/views/page.php');
        }
        else{
            $this->setView(APP_ROOT . '/views/errors/404.php');
        }

        return $this;
    }

    public function update(int $id, array $data)
    {

    }

    public function delete(int $id)
    {

    }
}
