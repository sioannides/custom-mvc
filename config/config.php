<?php
    return [
        "SITE_NAME" => $_ENV['SITE_NAME'],
        "DOMAIN" => $_ENV['DOMAIN'],
        "APP_ROOT" => dirname(dirname(__FILE__)),
        "URL_ROOT" => '/',
        "URL_SUBFOLDER" => '',
    ];
