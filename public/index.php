<?php

// Autoloader
require_once '../vendor/autoload.php';

try {
    $dotenv = Dotenv\Dotenv::createImmutable( dirname(__DIR__) );
    $dotenv->load();
}
catch (\Exception $e) {
    echo $e->getMessage();
    exit(1);
}
// Load Config
require_once dirname(__DIR__).'/app/SetConfig.php';

// Load Routes
$routeFiles = recursiveLoadFiles(dirname(__DIR__).'/routes/');
foreach ($routeFiles as $include) {
    if ($include->isReadable()) {
        require_once($include->getPathname());
    }
}

require_once dirname(__DIR__).'/app/Router.php';

