<?php

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

// Routes system
$routes = new RouteCollection();
$routes->add('page', new Route(constant('URL_SUBFOLDER') . '/{slug}',
    array('controller' => 'PageController', 'method'=>'showAction'),
    array('slug' => '[-A-Za-z0-9]+')));

$routes->add('index', new Route(constant('URL_SUBFOLDER') . '/',
    array('controller' => 'PageController', 'method'=>'indexAction')));
